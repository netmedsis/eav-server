﻿using ToSic.Eav.Documentation;

namespace ToSic.Eav.Data
{
    /// <summary>
    /// Content Type Constants
    /// </summary>
    [PrivateApi]
    public class ContentTypes
    {
        #region Metadata-Properties which have system use

        public const string ContentTypeTypeName = "ContentType";
        public static readonly string ContentTypeMetadataLabel = "Label";

        public static readonly string DynamicChildrenField = "DynamicChildrenField";

        #endregion
    }
}
