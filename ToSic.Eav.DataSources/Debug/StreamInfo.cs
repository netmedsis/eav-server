﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using ToSic.Eav.Conversion;

namespace ToSic.Eav.DataSources.Debug
{
    public class StreamInfo
    {
        public Guid Target;
        public Guid Source;
        public string SourceOut;
        public string TargetIn;
        public int Count => Stream.List.Count();
        
        
        public bool Error = false;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public IDictionary<string, object> ErrorData;
        
        [JsonIgnore]
        protected readonly IDataStream Stream;

        public StreamInfo(IDataStream stream, IDataTarget target, string inName, IEntitiesTo<IDictionary<string, object>> errorConverter)
        {
            try
            {
                Stream = stream;
                Target = (target as IDataSource)?.Guid ?? Guid.Empty;
                Source = stream.Source.Guid;
                TargetIn = inName;
                if (stream is ConnectionStream conStream1) SourceOut = conStream1.Connection.SourceStream;
                else
                    foreach (var outStm in stream.Source.Out)
                        if (outStm.Value == stream) // || (stream is ConnectionStream conStream && conStream.Connection.SourceStream == outStm.Key))
                            SourceOut = outStm.Key;

                var firstItem = Stream.List?.FirstOrDefault();
                Error = firstItem?.Type?.Name == DataSourceErrorHandling.ErrorType;
                if (Error) ErrorData = errorConverter.Convert(firstItem);
            }
            catch
            {
                Error = true;
            }
        }


        public bool Equals(StreamInfo comparison) =>
            comparison.Target == Target
            && comparison.Source == Source
            && comparison.TargetIn == TargetIn
            && comparison.SourceOut == SourceOut;
    }
}
