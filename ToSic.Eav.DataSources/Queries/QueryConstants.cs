﻿namespace ToSic.Eav.DataSources.Queries
{
    public class QueryConstants
    {
        public const string PartAssemblyAndType = "PartAssemblyAndType";
        public const string VisualDesignerData = "VisualDesignerData";

        public const string ParamsLookup = "Params";

        public const string ParamsShowDraftKey = "ShowDrafts";

        public const string ParamsShowDraftToken = "[Settings:ShowDrafts||false]";
    }
}